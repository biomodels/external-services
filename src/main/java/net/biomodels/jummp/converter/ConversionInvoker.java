package net.biomodels.jummp.converter;

import net.biomodels.jummp.converter.request.ConversionRequest;

public interface ConversionInvoker {

    boolean convert(ConversionRequest request);
}
