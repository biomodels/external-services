package net.biomodels.jummp.converter.candidates;

import net.biomodels.jummp.converter.ConversionInvoker;
import net.biomodels.jummp.converter.ModelFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Scope("prototype")
public class SbmlConversionStrategyBuilder {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private ModelFormat outFormat;

    private Sbml2AnyReliedOnSbfcWs remoteSbfcSbmlConverter;

    @Autowired
    public void setRemoteSbfcSbmlConverter(Sbml2AnyReliedOnSbfcWs remoteSbfcSbmlConverter) {
        this.remoteSbfcSbmlConverter = remoteSbfcSbmlConverter;
    }

    public ConversionInvoker build() {
        Objects.requireNonNull(outFormat, "Please specify the target format of the conversion.");
        ConversionInvoker result;
        switch (outFormat.getName().toLowerCase()) { // TODO deal with format versions
            case "sbml":
                throw new UnsupportedOperationException();
            case "biopax":
                result = remoteSbfcSbmlConverter;//new Sbml2BioPax();
                break;
            case "xpp":
                result = new Sbml2Xpp();
                break;
            case "svg":
                result = new Sbml2Svg();
                break;
            case "pdf":
                throw new UnsupportedOperationException();
            case "octave":
                result = new Sbml2Octave();
                break;
            default:
                result = remoteSbfcSbmlConverter;
                break;
        }
        logger.debug("Using conversion invoker {} to generate output format {}.",
                result, outFormat);
        return result;
    }


    public SbmlConversionStrategyBuilder outFormat(ModelFormat outFormat) {
        this.outFormat = outFormat;
        return this;
    }
}
