package net.biomodels.jummp.converter.request;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;

import java.util.concurrent.ConcurrentHashMap;

@ApplicationScope
@Service
public class SimpleConversionRequestCacheService {
    private final ConcurrentHashMap<Object, Object> requestCache;

    public SimpleConversionRequestCacheService() {
        requestCache = new ConcurrentHashMap<>(512, 0.9f);
    }

}