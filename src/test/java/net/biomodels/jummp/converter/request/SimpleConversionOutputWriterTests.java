package net.biomodels.jummp.converter.request;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class SimpleConversionOutputWriterTests {

    private final SimpleConversionOutputWriter writer = new SimpleConversionOutputWriter();

    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testSaveOutputToFile() throws IOException {
        tmpFolder.create();
        Path root = tmpFolder.getRoot().toPath();
        Path expectedOutputFile = root.resolve("dummy.out");

        Path output = writer.saveOutputToFile("DUMMY", expectedOutputFile.toString());
        assertEquals(output, expectedOutputFile);

        List<String> lines = Files.readAllLines(output);
        assertEquals(1, lines.size());
        assertEquals("DUMMY", lines.get(0));
    }

    @Test
    public void testSaveNonAsciiOutputToFile() throws IOException {
        tmpFolder.create();
        File out = tmpFolder.newFile();
        String lf = System.getProperty("line.separator");
        String line1 = String.valueOf((char) 0x03B4); // lowercase delta
        String line2 = "hello";
        String msg = line1 + lf + line2;

        writer.saveOutputToFile(msg, out.toString());

        List<String> output = Files.readAllLines(out.toPath());
        assertEquals(2, output.size());
        assertEquals(line1, output.get(0));
        assertEquals(line2, output.get(1));
    }

    @Test
    public void testSaveOutputToEmptyPath() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(CoreMatchers.endsWith("is not a writable file."));

        writer.saveOutputToFile("", "");
    }

    @Test
    public void testSaveOutputToDummyPath() throws IOException {
        tmpFolder.create();
        File root = tmpFolder.getRoot();
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(CoreMatchers.endsWith("is not a writable file."));

        writer.saveOutputToFile("", root.toString());
    }
}
