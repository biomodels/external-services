package net.biomodels.jummp.converter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

public class FormatExtensionRegistry {
    public static final String DEFAULT_EXTENSION = ".out";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Map<String, String> mapping;

    public FormatExtensionRegistry() {
        mapping = new LinkedHashMap<>();
    }

    public FormatExtensionRegistry(Map<String, String> mapping) {
        this.mapping = Collections.unmodifiableMap(mapping);
    }

    public String getExtensionForFormat(@NotNull ModelFormat format) {
        String name = format.getName();
        Map<String, String> newMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        newMap.putAll(mapping);
        String result = newMap.get(name);
        if (null == result) {
            logger.error("Cannot find extension for format '{}'", name);
            result = DEFAULT_EXTENSION;
        }
        return result;
    }

    public Set<String> getAvailableFormats() {
        return mapping.keySet();
    }

    public Map<String, String> mapSupportedFormats() {
        Map<String, String> swapped = mapping.
                entrySet().
                stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
        return swapped;
    }
}
