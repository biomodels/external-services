package net.biomodels.jummp.converter.candidates;

import net.biomodels.jummp.converter.ConversionInvoker;
import net.biomodels.jummp.converter.request.ConversionRequest;
import org.sbfc.converter.exceptions.ConversionException;
import org.sbfc.converter.exceptions.ReadModelException;
import org.sbfc.converter.exceptions.WriteModelException;
import org.sbfc.converter.models.SBMLModel;
import org.sbfc.converter.models.XPPModel;
import org.sbfc.converter.sbml2xpp.SBML2XPP;

import java.nio.file.Path;

public class Sbml2Xpp extends GeneralCandidate implements ConversionInvoker {
    @Override
    public boolean convert(ConversionRequest request) {
        Path inputFile = request.getInputFile();
        SBMLModel sbml = resolveSbmlModel(inputFile.toString());
        if (null == sbml) {
            throw new IllegalArgumentException("Cannot read SBML model for " + request);
        }
        SBML2XPP converter = new SBML2XPP();
        try {
            XPPModel xppModel = converter.xppExport(sbml);
            xppModel.modelToFile(request.getResult().getOutputFile().toString());
            return true;
        } catch (ConversionException e) {
            throw new IllegalStateException("Could not run SBML2XPP for " + request, e);
        } catch (ReadModelException e) {
            throw new IllegalArgumentException(e);
        } catch (WriteModelException e) {
            throw new IllegalStateException("Cannot save XPP export for " + request, e);
        }
    }
}
