package net.biomodels.jummp.converter;

import net.biomodels.jummp.converter.candidates.SbmlConversionStrategyBuilder;
import net.biomodels.jummp.converter.request.ConversionRequest;
import net.biomodels.jummp.converter.util.RequestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("jummpConversionService") // avoid clash with Spring's eponymous bean
public class ConversionService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final FormatExtensionRegistry formatExtensionRegistry;
    private final SbmlConversionStrategyBuilder sbmlConversionStrategyBuilder;

    public ConversionService(FormatExtensionRegistry formatExtensionRegistry,
            SbmlConversionStrategyBuilder sbmlConversionStrategyBuilder) {
        this.formatExtensionRegistry = formatExtensionRegistry;
        this.sbmlConversionStrategyBuilder = sbmlConversionStrategyBuilder;
    }

    // TODO cache result using Spring Cache or our own SimpleConversionRequestCacheService
    public boolean handle(ConversionRequest request) {
        Objects.requireNonNull(request);

        ConversionInvoker conversionStrategy = sbmlConversionStrategyBuilder
                .outFormat(request.getTo())
                .build();

        String outputExtension = formatExtensionRegistry.getExtensionForFormat(request.getTo());
        String inputFile = request.getInputFile().toString();
        String outputFilePath = RequestUtils.replaceFileExtension(inputFile, outputExtension);

        request.setConvertedFileLocation(outputFilePath);
        logger.debug("Writing output to {}", outputFilePath);
        return conversionStrategy.convert(request);

    }
}
