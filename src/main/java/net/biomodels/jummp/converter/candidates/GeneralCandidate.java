package net.biomodels.jummp.converter.candidates;

import net.biomodels.jummp.converter.request.ConversionRequest;
import org.sbfc.converter.exceptions.ReadModelException;
import org.sbfc.converter.models.SBMLModel;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;

@SuppressWarnings("WeakerAccess")
public class GeneralCandidate {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Resolves the SBMLModel from a specified location.
     * @param modelFilePath The location from which to extract the SBMLModel. Must not be null.
     * @return The {@link SBMLModel} representation of the given file.
     */
    public SBMLModel resolveSbmlModel(@NotNull String modelFilePath) {
        SBMLModel sbmlModel = new SBMLModel();
        SBMLDocument parsedDocument;
        try {
            logger.debug("Parsing {} with JSBML...", modelFilePath);
            parsedDocument = sbmlModel.modelFromFile(modelFilePath);
            logger.debug("JSBML parsing of {} complete", modelFilePath);
        } catch (ReadModelException e) {
            throw new IllegalArgumentException("Cannot read SBML model " + modelFilePath, e);
        }
        if (null != parsedDocument) {
            return sbmlModel;
        } else {
            throw new IllegalStateException("Internal JSBML error while reading " + modelFilePath);
        }
    }

    protected String getInputFileForRequest(@NotNull ConversionRequest request) {
        return request.getInputFile().toString();
    }
}
