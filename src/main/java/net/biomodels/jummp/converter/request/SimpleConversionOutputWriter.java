package net.biomodels.jummp.converter.request;

import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class SimpleConversionOutputWriter {
    public Path saveOutputToFile(@NotNull String output, @NotNull String filePath) {
        Path path = Paths.get(filePath);
        try (FileOutputStream fos = new FileOutputStream(path.toFile(), false);
                OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
                BufferedWriter bw = new BufferedWriter(osw)) {
            bw.write(output);
            return path;
        } catch (IOException e) {
            throw new IllegalArgumentException(filePath + " is not a writable file.");
        }
    }
}
