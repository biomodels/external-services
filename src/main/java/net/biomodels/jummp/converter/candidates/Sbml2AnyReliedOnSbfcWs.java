package net.biomodels.jummp.converter.candidates;

import net.biomodels.jummp.converter.ConversionInvoker;
import net.biomodels.jummp.converter.ModelFormat;
import net.biomodels.jummp.converter.request.ConversionRequest;
import net.biomodels.jummp.converter.request.SimpleConversionOutputWriter;
import org.sbfc.ws.SBFCWebServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("remoteSbfcSbmlConverter")
public class Sbml2AnyReliedOnSbfcWs extends GeneralCandidate implements ConversionInvoker {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final SBFCWebServiceClient sbfcWebServiceClient;

    private final SimpleConversionOutputWriter conversionOutputWriter;

    public Sbml2AnyReliedOnSbfcWs(SBFCWebServiceClient sbfcWebServiceClient,
            SimpleConversionOutputWriter writer) {
        this.sbfcWebServiceClient = sbfcWebServiceClient;
        this.conversionOutputWriter = writer;
    }

    @Override
    public boolean convert(ConversionRequest request) {
        String from = request.getFrom().getName();
        if (!"sbml".equals(from)) {
            throw new IllegalArgumentException("Remote SBFC converter does not support " + from);
        }
        String conversionResult;
        String outputModel = request.getResult().getOutputFile().toString();
        String inputType = "SBMLModel";
        String converter = translateOutputFormat(request.getTo());
        logger.info("Launching {} conversion job for revision {}", converter, request.getRevision());

        try {
            String in = getInputFileForRequest(request);
            conversionResult = sbfcWebServiceClient.submitAndGetResultFromFile(in, inputType, converter);
            conversionOutputWriter.saveOutputToFile(conversionResult, outputModel);
            return true;
        } catch (InterruptedException e) {
            String m = "Interrupted while converting '" + request + "' to " + outputModel;
            throw new IllegalStateException(m, e);
        }
    }

    private String translateOutputFormat(ModelFormat format) {
        String prefix = "SBML2";
        String suffix;
        switch (format.getName()) {
            case "xpp":
                suffix = "XPP";
                break;
            case "octave":
                suffix = "Octave";
                break;
            case "biopax":
                suffix = "BioPAX_l3";
                break;
            default:
                throw new UnsupportedOperationException("target format not supported by sbfc yet");
        }
        return prefix + suffix;
    }


}
