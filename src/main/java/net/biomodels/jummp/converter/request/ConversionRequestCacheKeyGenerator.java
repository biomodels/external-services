package net.biomodels.jummp.converter.request;

import net.biomodels.jummp.converter.ModelFormat;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.lang.reflect.Method;

/**
 * Custom KeyGenerator implementation for use with {@link ConversionRequest} caches.
 *
 * Cache keys are stored on the heap, so the smaller the keys, the lower the memory requirements
 * and thus the better for the garbage collector.
 *
 * This service can be used to cache methods whose first argument is a ConversionRequest.
 */
@Component
public class ConversionRequestCacheKeyGenerator implements KeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {
        Assert.notEmpty(params, String.format("Method %s should have a ConversionRequest param",
                method.toGenericString()));
        Assert.isInstanceOf(ConversionRequest.class, params[0]);
        ConversionRequest request = (ConversionRequest) params[0];
        long revision = request.getRevision();
        ModelFormat targetFormat = request.getTo();
        String inputFileName = request.getInputFile().getFileName().toString();
        return new SimpleKey(revision, targetFormat, inputFileName);
    }
}


