package net.biomodels.jummp;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import net.biomodels.jummp.converter.FormatExtensionRegistry;
import org.sbfc.ws.SBFCWebServiceClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

import java.util.Map;
import java.util.TreeMap;

@SpringBootApplication
@EnableCaching
public class MainApplication {
    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    /**
     * Stateless singleton for interacting with the Web Service endpoints exposed by SBFC.
     */
    @Bean
    public SBFCWebServiceClient sbfcWebServiceClient() {
        return new SBFCWebServiceClient();
    }

    @Bean
    public FormatExtensionRegistry formatExtensionRegistry() {
        Map<String, String> formatExtensionMapping = new TreeMap<>();
        formatExtensionMapping.put("Octave", ".m");
        formatExtensionMapping.put("XPP", ".xpp");
        formatExtensionMapping.put("BioPAX", ".owl");
        return new FormatExtensionRegistry(formatExtensionMapping);
    }

    /**
     * The Hazelcast configuration.
     */
    @Bean
    public Config config() {
        Config config = new Config();
        config.setProperty("hazelcast.http.healthcheck.enabled ", "true");
        config.setProperty("hazelcast.jmx", "true");
        config.setProperty("hazelcast.logging.type","slf4j");
        config.setProperty("hazelcast.phone.home.enabled", "false");
        config.setProperty("hazelcast.rest.enabled", "true");
        return config;
    }

    /**
     * The Hazelcast instance.
     * Used by Spring to create a {@link com.hazelcast.spring.cache.HazelcastCacheManager} as well.
     */
    @Bean
    public HazelcastInstance hazelcastInstance() {
        return Hazelcast.newHazelcastInstance(config());
    }
}
