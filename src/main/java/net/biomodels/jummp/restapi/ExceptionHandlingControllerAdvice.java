package net.biomodels.jummp.restapi;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(basePackages = "net.biomodels.jummp.restapi")
public class ExceptionHandlingControllerAdvice extends ResponseEntityExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @SuppressWarnings({"unchecked", "unused"})
    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<JSONObject> handleControllerException(HttpServletRequest request,
            Throwable e) {
        JSONObject jsonObject = new JSONObject();
        String msg = String.format("Request %s produced exception %s", request.getRequestURI(), e);
        jsonObject.put("message", msg);
        HttpStatus responseCode = getResponseCodeByErrorType(e);
        if (responseCode.is4xxClientError()) {
            logger.info(msg, e);
        } else if (responseCode.is5xxServerError()) {
            logger.error(msg, e);
        }
        return new ResponseEntity<>(jsonObject, responseCode);
    }

    private HttpStatus getResponseCodeByErrorType(Throwable e) {
        HttpStatus status;

        boolean isIllegalArgument = e instanceof IllegalArgumentException;
        boolean isUnsupported = e instanceof UnsupportedOperationException;

        if (isIllegalArgument) {
            status = HttpStatus.BAD_REQUEST;
        }
        else if (isUnsupported) {
            status = HttpStatus.NOT_IMPLEMENTED;
        }
        else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return status;
    }
}
