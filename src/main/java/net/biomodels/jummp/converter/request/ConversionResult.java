package net.biomodels.jummp.converter.request;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConversionResult implements DataSerializable {
    private Path outputFile;

    public ConversionResult() {}

    public ConversionResult(Path outputFile) {
        this.outputFile = outputFile;
    }

    public void setOutputFile(Path outputFile) {
        this.outputFile = outputFile;
    }

    public String getOutputFileUri() {
        return outputFile.toUri().toString();
    }

    public Path getOutputFile() {
        return outputFile;
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeUTF(getOutputFileUri());
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        String fileUri = null;
        try {
            fileUri = in.readUTF();
            outputFile = Paths.get(new URI(fileUri));
        } catch (URISyntaxException e) {
            throw new IOException(fileUri, e);
        }
    }
}
