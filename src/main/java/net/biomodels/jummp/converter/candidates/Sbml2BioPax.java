package net.biomodels.jummp.converter.candidates;

import net.biomodels.jummp.converter.ConversionInvoker;
import net.biomodels.jummp.converter.request.ConversionRequest;
import org.sbfc.converter.exceptions.WriteModelException;
import org.sbfc.converter.models.BioPAXModel;
import org.sbfc.converter.models.SBMLModel;
import org.sbfc.converter.sbml2biopax.SBML2BioPAX_l2;

import javax.validation.constraints.NotNull;

public class Sbml2BioPax extends GeneralCandidate implements ConversionInvoker {
    @Override
    public boolean convert(@NotNull ConversionRequest request) {
        String inputFile = request.getInputFile().toString();
        String outputFile = request.getResult().getOutputFile().toString();
        SBMLModel sbmlModel = resolveSbmlModel(inputFile);
        BioPAXModel bioPaxModel = new SBML2BioPAX_l2().biopaxexport(sbmlModel);
        try {
            bioPaxModel.modelToFile(outputFile);
            return true;
        } catch (WriteModelException e) {
            throw new IllegalStateException("Cannot save BioPAX L2 export for " + request, e);
        }
    }
}
