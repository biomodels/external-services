package net.biomodels.jummp.converter.util;

import javax.validation.constraints.NotNull;


public abstract class RequestUtils {
    private static final char DOT = '.';

    public static String replaceFileExtension(String inputName, String newExtension) {
        return replaceFileExtension(inputName, newExtension, false);
    }

    public static String replaceFileExtension(@NotNull String inputName, @NotNull String newExtension,
            boolean preserveLeadingDot) {
        if (inputName.isEmpty())
            throw new IllegalArgumentException("Input conversion file cannot have empty name.");
        int lastDot = inputName.lastIndexOf(DOT);
        String prefix;
        if (-1 == lastDot || (0 == lastDot && preserveLeadingDot)) {
            prefix = inputName;
        } else {
            prefix = inputName.substring(0, lastDot);
        }
        String newFileName = prefix +
                (newExtension.indexOf(DOT) == 0 ? newExtension : DOT + newExtension);
        return newFileName;
    }
}
