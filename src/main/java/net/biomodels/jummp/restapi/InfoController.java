package net.biomodels.jummp.restapi;

import net.biomodels.jummp.converter.FormatExtensionRegistry;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
public class InfoController {

    private FormatExtensionRegistry extensionRegistry;

    public InfoController(FormatExtensionRegistry extensionRegistry) {
        this.extensionRegistry = extensionRegistry;
    }

    @RequestMapping(value = "/info/targetFormats", method = RequestMethod.GET,
            produces = "application/json;charset=UTF-8")
    public ResponseEntity<Set<String>> targetFormats() {
        Set<String> formats = extensionRegistry.getAvailableFormats();
        return new ResponseEntity<>(formats, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/info/mapSupportedFormats", method = RequestMethod.GET,
            produces = "application/json;charset=UTF-8")
    public ResponseEntity<Map<String, String>> mapSupportedFormats() {
        Map<String, String> formats = extensionRegistry.mapSupportedFormats();
        return new ResponseEntity<>(formats, new HttpHeaders(), HttpStatus.OK);
    }
}
