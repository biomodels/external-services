package net.biomodels.jummp.converter.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.runners.Parameterized.Parameter;

@RunWith(Parameterized.class)
public class RequestUtilsTests {

    @Parameterized.Parameters
    public static Collection<String[]> data() {
        return Arrays.asList(new String[][] {
                {"foo.txt", ".bar", "foo.bar"},
                {"foo.txt", "bar", "foo.bar"},
                {"foo", "bar", "foo.bar"},
                {"foo.bar.txt", "baz", "foo.bar.baz"},
                {".foo.txt", "bar", ".foo.bar"},
                {".foo", "bar", ".foo.bar"},
                {".foo", ".", ".foo."},
                {".foo", "", ".foo."}
        });
    }

    @Parameter
    public String orig;
    @Parameter(1)
    public String ext;
    @Parameter(2)
    public String expected;

    @Test()
    public void testReplaceFileExtensions() {
        assertEquals(expected, RequestUtils.replaceFileExtension(orig, ext, true));
        Throwable iae = new IllegalArgumentException("Input conversion file cannot have empty name.");
        try {
            RequestUtils.replaceFileExtension("", "");
            fail();
        } catch (Throwable actual) {
            assertEquals(iae.toString(), actual.toString());
        }
        assertEquals(".bar", RequestUtils.replaceFileExtension(".foo", "bar"));
    }
}
