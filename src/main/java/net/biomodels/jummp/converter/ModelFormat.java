package net.biomodels.jummp.converter;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;
import java.io.Serializable;

public class ModelFormat implements DataSerializable, Serializable {
    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_VERSION = "*";
    private String name;

    private String version;

    public ModelFormat() {}

    public ModelFormat(String name) {
        this(name, DEFAULT_VERSION);
    }

    public ModelFormat(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String toString() {
        return DEFAULT_VERSION.equals(version) ? name : name + ' ' + version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ModelFormat that = (ModelFormat) o;

        return name.equals(that.name) && version.equals(that.version);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeUTF(name);
        out.writeUTF(version);
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        name = in.readUTF();
        version = in.readUTF();
    }
}